const router = require('express-promise-router')()

import { signUp, signIn, refreshTokens } from '../controllers/users'

router.post(
  '/sign-up',
  signUp
)

router.post(
  '/sign-in',
  signIn
)

router.post(
  '/refresh-token',
  refreshTokens
)

export default router
