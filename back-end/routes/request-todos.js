const router = require('express-promise-router')()

import checkAuth from '../midlleware/auth'

import { getAll,
  addTodo,
  deleteTodo,
  updateTodo,
  getSingle,
  searchTodo,
  filterTodo,
  toggleAll } from '../controllers/todos'

router.get(
  '/getAll',
  checkAuth,
  getAll
)

router.get(
  '/getSingle/:id',
  checkAuth,
  getSingle
)

router.post(
  '/addTodo',
  checkAuth,
  addTodo
)

router.delete(
  '/deleteTodo/:id',
  checkAuth,
  deleteTodo
)

router.put(
  '/updateTodo/:id',
  checkAuth,
  updateTodo
)

router.post(
  '/search',
  checkAuth,
  searchTodo
)

router.post(
  '/filter',
  checkAuth,
  filterTodo
)

router.put(
  '/toggleAll',
  checkAuth,
  toggleAll
)

export default router
