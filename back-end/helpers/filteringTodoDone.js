export const filteringTodoDone = (todosList, filterParam) => {
    return todosList.filter((item) => {
      switch(filterParam) {
        case 'ALL': {
          return item
        }
        case 'ACTIVE': {
          return item.done
        }
        case 'NO_ACTIVE': {
          return !item.done
        }
      }
    })  
}