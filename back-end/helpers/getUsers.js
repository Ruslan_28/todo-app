import jwt from 'jsonwebtoken'
import Todo from '../models/Todo'

export const getUsers = (request) => {
    const { authorization } = request.headers
    const decoded = jwt.decode(authorization)
    const userId = decoded.userId

    const todos = Todo.find({userId: userId})

    return todos
}

export const getUserId = (authorization) => {
  const decoded = jwt.decode(authorization)
  const userId = decoded.userId

  return userId
}
