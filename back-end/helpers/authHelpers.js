import jwt from 'jsonwebtoken'
import uuid from 'uuid/v4'
import Token from '../models/Token'

export const generateAccesToken = (userId) => {
    const payload = {
        userId,
        type: 'access'
    }

    const option = { expiresIn: '2day' }

    return jwt.sign(payload, 'secret', option)
}

export const generateRefreshToken = () => {
    const payload = {
        id: uuid(),
        type: 'refresh'
    }

    const option = { expiresIn: '3day' }

    return {
        id: payload.id,
        token: jwt.sign(payload, 'secret', option)
    }
}

export const replaceDbRefreshToken = (tokenId, userId) => 
    Token.findOneAndRemove({ userId })
        .exec()
        .then(() => Token.create({ tokenId, userId }))
