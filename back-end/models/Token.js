import mongoose from 'mongoose'

const Schema = mongoose.Schema

const tokenSchema = new Schema({
  tokenId:  String,
  UserId: String,
})

const Token = mongoose.model('Token', tokenSchema)

export default Token