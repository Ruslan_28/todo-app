import mongoose from 'mongoose'
const bcrypt = require('bcrypt')

const Schema = mongoose.Schema

const userSchema = new Schema({
  username: String,
  email:  {type: String, unique: true},
  password: String
})

userSchema.pre('save', function(next) {
  let self = this

  if (!self.isModified('password')) {
    return next()
  }

  bcrypt
    .genSalt(12)
    .then((salt) => {
      return bcrypt.hash(self.password, salt)
    })
    .then((hash) => {
      self.password = hash
      next()
    })
    .catch((err) => next(err))
})

const User = mongoose.model('User', userSchema)

export default User
