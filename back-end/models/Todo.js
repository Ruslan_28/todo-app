import mongoose from 'mongoose'

const Schema = mongoose.Schema

const todoSchema = new Schema({
  title:  String,
  description: String,
  done: Boolean,
  userId: String,
  date: { type: Date, default: new Date() },
})

const Todo = mongoose.model('Todo', todoSchema)

export default Todo
