import Todo from '../models/Todo'
import { getUsers, getUserId } from '../helpers/getUsers'
import { filteringTodoDone } from '../helpers/filteringTodoDone'

export const getAll = async (request, response) => {
  const todos = await getUsers(request)

  response.json(todos)
}

export const getSingle = async (request, response) => {
  const todo = await Todo.findOne({ _id: request.params.id })

  response.json(todo)
}

export const addTodo = async (request, response) => {
  const todo = await new Todo({ title: request.body.title, description: request.body.description, userId: request.body.userId,done: false, date: request.body.date }).save()

  response.json(todo)
}

export const deleteTodo = async (request, response) => {
  const todos = await Todo.findOne({ _id: request.params.id }).deleteOne()

  response.json(todos)
}

export const updateTodo = async (request, response) => {
  const todos = await Todo.findByIdAndUpdate(request.params.id, request.body, {new: true})

  response.json(todos)
}

export const searchTodo = async (request, response) => {
  const todosList = await getUsers(request)

  const todos = todosList.filter(item => {
    return item.title.toLowerCase().includes(request.body.title.toLowerCase())
  })

  response.json(todos)
}

export const filterTodo = async (request, response) => {
  const todosList = await getUsers(request)

  let filterParam = request.body.filter

  const todos = filteringTodoDone(todosList, filterParam)

  response.json(todos)
}

export const toggleAll = async (request, response) => {
  const userId = await getUserId(request.headers.authorization)

  const todos = await Todo.updateMany({userId}, {$set : request.body })

  response.json(todos)
}
