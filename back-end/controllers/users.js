import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import Joi from "joi"
import User from '../models/User'
import Token from '../models/Token'
import { generateAccesToken, generateRefreshToken, replaceDbRefreshToken } from '../helpers/authHelpers'

const updateTokens = (userId) => {
  const accesToken = generateAccesToken(userId)
  const refreshToken = generateRefreshToken()

  return replaceDbRefreshToken(refreshToken.id, userId)
    .then(() => ({
      accesToken,
      refreshToken: refreshToken.token
    })) 
}

const schema = Joi.object().keys({
  username: Joi.string().min(2).max(30),
  email: Joi.string().email().min(7).max(30),
  password: Joi.string().min(4).max(30),
})

export const signUp = async (request, response) => {
  const validated = Joi.validate(request.body, schema)

  if(validated.error) {
    return response.status(422).json(validated.error)
  } else {
    try {
      response.json(await User.create(validated.value))
    } catch (err) {
      response.status(422).json( {details: [{message: 'Invalid Email', err}]} )
    }
    
  }
}

export const signIn = async (request, response) => {
  const { email, password } = request.body

  const user = await User.findOne({ email })

  if(!user) {
    response.status(401).json({ message: 'User does not exist' })
  }

  const isValid = bcrypt.compareSync(password, user.password)
  
  if(isValid) {
    const tokens = await updateTokens(user._id)
    response.json({tokens, name: user.username, userId: user._id})
  } 
  else {
    response.status(401).json({ message: 'invalid cred' })
  }
}

export const refreshTokens = async (request, response) => {
  const { refreshToken } = request.body
  let payload
  
  try {
    payload = jwt.verify(refreshToken, 'secret')
    if(payload.type !== 'refresh') {
      response.status(400).json({ message: 'Invalid token' })
      return
    }
  }
  catch(e) {
    if(e instanceof jwt.TokenExpiredError) {
      response.status(400).json({ message: 'Invalid expired' })
      return
    } else if(e instanceof jwt.JsonWebTokenError) {
      response.status(400).json({ message: 'Invalid token' })
      return
    }
  }

  const token = await Token.findOne({ tokenId: payload.id })

  if(token === null) {
    throw new Error('invalid token')
  }

  const tokens = await updateTokens(token.userId) 

  response.json(tokens)
}