import jwt from 'jsonwebtoken'

const checkAuth = (request, response, next) => {
    const token = request.headers['authorization']

    if(!token) {
        console.log('Token is not provided')
        response.status(401).json({ok: false, message: 'Token is not provided'})
    } else {
        jwt.verify(token, 'secret', (err, decoded) => {
        if(err) {
            console.log('Invalid token')
            response.status(401).json({ok: false, message: 'Invalid token'})
        } else {
            request.email = decoded.email
            next()
        }
        })
    }
}

export default checkAuth