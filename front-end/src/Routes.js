import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import FormTodos from './pages/FormTodos'
// import TodosLists from './pages/TodosLists'
import HomeTodos from './pages/HomeTodos'
import TodoDescription from './pages/TodoDescription'


const Routers = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomeTodos} />
        <Route exact path="/add" component={FormTodos} />
        {/* <Route exact path="/" component={TodosLists} /> */}
        <Route exact path="/description/:id" component={TodoDescription} />
      </Switch>
    </Router>
  );
}

export default Routers;
