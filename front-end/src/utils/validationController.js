import isEmail from 'validator/lib/isEmail';

export const validationName = (name, required = true) => {
    if (!name) {
        return required ? 'Required' : undefined;
    }
    if (!isValidName(name)) {
        return 'Name is incorrect';
    }
    return undefined;
};

const isValidName = (name) => {
    const pattern = /^(?!\s)[a-zA-ZÆÐƎƏƐƔĲŊŒẞÞǷȜæðǝəɛɣĳŋœĸſßþƿȝĄƁÇĐƊĘĦĮƘŁØƠŞȘŢȚŦŲƯY̨Ƴąɓçđɗęħįƙłøơşșţțŧųưy̨ƴÁÀÂÄǍĂĀÃÅǺĄÆǼǢƁĆĊĈČÇĎḌĐƊÐÉÈĖÊËĚĔĒĘẸƎƏƐĠĜǦĞĢƔáàâäǎăāãåǻąæǽǣɓćċĉčçďḍđɗðéèėêëěĕēęẹǝəɛġĝǧğģɣĤḤĦIÍÌİÎÏǏĬĪĨĮỊĲĴĶƘĹĻŁĽĿʼNŃN̈ŇÑŅŊÓÒÔÖǑŎŌÕŐỌØǾƠŒĥḥħıíìiîïǐĭīĩįịĳĵķƙĸĺļłľŀŉńn̈ňñņŋóòôöǒŏōõőọøǿơœŔŘŖŚŜŠŞȘṢẞŤŢṬŦÞÚÙÛÜǓŬŪŨŰŮŲỤƯẂẀŴẄǷÝỲŶŸȲỸƳŹŻŽẒŕřŗſśŝšşșṣßťţṭŧþúùûüǔŭūũűůųụưẃẁŵẅƿýỳŷÿȳỹƴźżžẓ\d\s\.'-]+$/u;
    return pattern.test(name);
};

export const validationEmail = (email, required = true) => {
    if (!email) {
        return required ? 'Required' : undefined;
    }
    if (!isEmail(email)) {
        return 'Email is wrong';
    }
    return undefined;
};

export const validationPassword = (password) => {
    if (!password) {
        return 'Required';
    }
    if (password.trim().length < 6) {
        return 'Password should contain at least 6 characters';
    }
    return undefined;
};

export const validationConfirmPassword = (password, confirmPassword) => {
    if (password !== confirmPassword) {
        return 'Passwords do not match';
    }
    if (!confirmPassword) {
        return 'Required';
    }
    return undefined;
};
