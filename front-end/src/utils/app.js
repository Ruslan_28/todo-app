export const pathTodos = 'http://localhost:8000/api/todos'
export const pathUsers = 'http://localhost:8000/api/users/'

export const STATE_STATUSES = {
    READY: 'ready',
    PENDING: 'pending',
    ERROR: 'error',
    INIT:'init',
};