import React, { useState } from 'react'
import { connect } from 'react-redux'
import './index.css'

import { requestSignUp, requestSignIn } from '../store/todos-notification/action-notification'

import SignUp from '../components/SignUp'
import SignIn from '../components/SignIn'
import Header from '../components/Header'

const Notification = (props) => {
    const [stateForm, toggleForm] = useState(true)

    return (
        <div>
            <Header />
            <div className="form-auth-box">
                <div className="toggle-button">
                    <button className={`${stateForm ? 'active-button' : ''}`} onClick={() => toggleForm(true)}>SignUp</button>
                    <button className={`${stateForm ? '' : 'active-button'}`} onClick={() => toggleForm(false)}>SignIn</button>
                </div>
                {
                    stateForm
                    ?
                    <SignUp
                        requestSignUp={props.requestSignUp}
                        errorMessage={props.errMessageUp}
                    />
                    :
                    <SignIn
                        requestSignIn={props.requestSignIn}
                        errorMessage={props.errMessageIn}
                    />
                }
            </div>
        </div>
    )
}

export default connect(
    state => ({
        errMessageUp: state.reducerNotification.errMessageUp,
        errMessageIn: state.reducerNotification.errMessageIn,
    }), { requestSignUp, requestSignIn }
)(Notification)
