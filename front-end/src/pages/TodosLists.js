import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components';
import { fetchAllTodos } from '../store/todos-main/action-main'
import Filters from '../components/Filters'
import Search from '../components/Search'
import Select from '../components/Select'
import TodoList from '../components/TodoList'
import Header from '../components/Header'

const HeaderWrapper = styled.div`
  max-width: 600px;
  margin: 0 auto;
  margin-top: 25px;

  .header-wrapper {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 25px;
  }
  
`

class TodoLists extends Component {
  componentDidMount() {
    this.props.fetchAllTodos()
  }

  render() {
    return (
      <>
        <Header />
        <HeaderWrapper>
          <>
            <div className="header-wrapper">
              <Filters />
              <Search />
            </div>  
            <Select />
          </>
          <TodoList />
        </HeaderWrapper>
      </>
    )
  }
}

export default connect(
  null, { fetchAllTodos }
)(TodoLists)
