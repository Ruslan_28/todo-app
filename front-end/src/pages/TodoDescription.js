import React, { Component } from 'react'

import Header from '../components/Header'
import Description from '../components/Description'

class TodoDescription extends Component {
    render() {
        return (
            <>
                <Header />
                <Description />
            </>
        )
    }
}

export default TodoDescription