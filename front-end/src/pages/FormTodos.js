import React from 'react'

import Form from '../components/Form'
import Header from '../components/Header'

export default () => (
  <>  
    <Header />
    <Form />
  </>
)
