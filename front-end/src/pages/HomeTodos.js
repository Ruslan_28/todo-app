import React from 'react'
import { connect } from 'react-redux'

import Notification from './Notification'
import TodosLists from './TodosLists'

const HomeTodos = (props) => props.isToken.length ? <TodosLists /> : <Notification />

export default connect(
    state => ({
      isToken: state.reducerNotification.isToken,
    })
)(HomeTodos)

