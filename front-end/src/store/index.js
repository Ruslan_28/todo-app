import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createSagaMiddleware from "redux-saga"
import { requestsPromiseMiddleware } from 'redux-saga-requests'
import { createLogger } from 'redux-logger'


import rootReducer from './rootReducer'
import rootSaga from './saga'

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  sagaMiddleware,
  requestsPromiseMiddleware(),
  thunk,
  createLogger(),
]

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(...middlewares),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)

sagaMiddleware.run(rootSaga)

export default store
