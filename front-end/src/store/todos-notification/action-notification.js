import {pathUsers} from '../../utils/app'

export const REQUEST_SIGN_UP = "REQUEST_SIGN_UP"

const signUp = (username, email, password) => {
  return {
    type: REQUEST_SIGN_UP,
    payload: {
      request: {
        method: 'POST',
        url: `${pathUsers}/sign-up`,
        data: { username, email, password }
      }
    },
    meta: {
      asPromise: true,
    },
  }
}

export const requestSignUp = (username, email, password) => async dispatch => {
  await dispatch(signUp(username, email, password))
  await dispatch(requestSignIn(email, password))
}

export const REQUEST_SIGN_IN = "REQUEST_SIGN_IN"

const signIn = (email, password) => {
  return {
    type: REQUEST_SIGN_IN,
    payload: {
      request: {
        method: 'POST',
        url: `${pathUsers}/sign-in`,
        data: { email, password }
      }
    },
    meta: {
      asPromise: true,
    },
  }
}

export const requestSignIn = (email, password) => async dispatch => {
  await dispatch(signIn(email, password))
    .then(response => {
      const { accesToken } = response.payload.data.tokens
      const { userId } = response.payload.data      
      localStorage.setItem('Token', accesToken)
      localStorage.setItem('userId', userId)
    })
}

export const REQUEST_LOG_OUT = "REQUEST_LOG_OUT"

export const requestLogOut = () => dispatch => {
  localStorage.setItem('Token', [])
  localStorage.setItem('userId', '')
  
  dispatch({ type: REQUEST_LOG_OUT })
}