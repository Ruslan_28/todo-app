import { success, error } from 'redux-saga-requests'
import { REQUEST_SIGN_IN, REQUEST_LOG_OUT, REQUEST_SIGN_UP } from './action-notification'

const initialState = {
    isToken: localStorage.getItem('Token') || [],
    userId: localStorage.getItem('userId') || '',
    errMessageUp: "",
    errMessageIn: ""
}

export default (state=initialState, action) => {
    switch(action.type) {
        case success(REQUEST_SIGN_IN): {
            const { userId } = action.payload.data
            const { accesToken } = action.payload.data.tokens

            return {
                ...state,
                isToken: accesToken,
                userId: userId,
                errMessageIn: ""
            }
        }
        case error(REQUEST_SIGN_IN): {
            const { message } = action.payload.response.data

            return {
                ...state,
                errMessageIn: message
            }
        }

        case error(REQUEST_SIGN_UP): {
            const [ message ] = action.payload.response.data.details
            const msg = message.message
            
            return {
                ...state,
                errMessageUp: msg
            }
        }

        case REQUEST_LOG_OUT: {
            return {
                ...state,
                isToken: [],
                userId: '',
            }
        }

        default: {
            return state
        }
    }
}
