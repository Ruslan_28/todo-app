import { combineReducers } from 'redux'

import reducerMain from './todos-main/reducer-main'
import reducerNotification from './todos-notification/reducer-notification'

export default combineReducers({
  reducerMain,
  reducerNotification
})
