import {pathTodos} from '../../utils/app'

export const FETCH_ALL_TODOS = 'FETCH_ALL_TODOS'
export const fetchAllTodos = () => {
  return {
    type: FETCH_ALL_TODOS,
    payload: {
      request: {
        method: 'GET',
        url: `${pathTodos}/getAll`,
      }
    },
    meta: {
      asPromise: true,
    },
  }
}

export const GET_SINGLE_TODO = 'GET_SINGLE_TODO'
export const getSingleTodo = (id) => {
  return {
    type: GET_SINGLE_TODO,
    payload: {
      request: {
        method: 'GET',
        url: `${pathTodos}/getSingle/${id}`,
      }
    }
  }
}

export const ADD_TODO = 'ADD_TODO'
export const addTodo = (title, description, userId) => {
  return {
    type: ADD_TODO,
    payload: {
      request: {
        method: 'POST',
        url: `${pathTodos}/addTodo`,
        data: { title, description, userId }
      }
    },
    meta: {
      asPromise: true,
    },
  }
}

export const DELETE_TODO = 'DELETE_TODO'
export const deleteTodo = (id) => {
  return {
    type: DELETE_TODO,
    payload: {
      request: {
        method: 'DELETE',
        url: `${pathTodos}/deleteTodo/${id}`,
      }
    },
    meta: {
      id
    }
  }
}

export const TOGGLE_TODO = 'TOGGLE_TODO'
export const toggleTodo = (id, done) => {
  return {
    type: TOGGLE_TODO,
    payload: {
      request: {
        method: 'PUT',
        url: `${pathTodos}/updateTodo/${id}`,
        data: { done: !done }
      }
    },
    meta: {
      id
    }
  }
}

export const EDIT_TODO = 'EDIT_TODO'
export const editTodo = (id, title, description) => {
  return {
    type: EDIT_TODO,
    payload: {
      request: {
        method: 'PUT',
        url: `${pathTodos}/updateTodo/${id}`,
        data: { title, description }
      }
    },
    meta: {
      id,
      title
    }
  }
}

export const TODO_FILTER = 'TODO_FILTER'
export const todoFilter = (filter) => {
  return {
    type: TODO_FILTER,
    payload: {
      request: {
        method: 'POST',
        url: `${pathTodos}/filter`,
        data: { filter }
      }
    }
  }
}

export const SEARCH_TODO = 'SEARCH_TODO'
export const searchTodo = (title) => {
  return {
    type: SEARCH_TODO,
    payload: {
      request: {
        method: 'POST',
        url: `${pathTodos}/search`,
        data: { title }
      }
    }
  }
}

export const SELECT_ALL = 'SELECT_ALL'
export const selectAll = () => {
  return {
    type: SELECT_ALL,
    payload: {
      request: {
        method: 'PUT',
        url: `${pathTodos}/toggleAll`,
        data: { done: true }
      }
    }
  }
}

export const RESELECT_ALL = 'RESELECT_ALL'
export const reselectAll = () => {
  return {
    type: RESELECT_ALL,
    payload: {
      request: {
        method: 'PUT',
        url: `${pathTodos}/toggleAll`,
        data: { done: false }
      }
    }
  }
}
