import { success, error } from 'redux-saga-requests'
import { FETCH_ALL_TODOS,
   ADD_TODO,
   DELETE_TODO,
   TOGGLE_TODO, 
   EDIT_TODO, 
   TODO_FILTER, 
   SEARCH_TODO, 
   GET_SINGLE_TODO, 
   SELECT_ALL,
   RESELECT_ALL } from './action-main'
import { STATE_STATUSES } from '../../utils/app'

const initialState = {
  todos: [],
  todo: {},
  isFiltering: 'ALL',
  status: STATE_STATUSES.INIT
}

export default (state = initialState, action) => {
  switch(action.type) {
    case FETCH_ALL_TODOS: {
      return processReducer(state)
    }
    case success(FETCH_ALL_TODOS): {
      const todos = action.payload.data

      return {
        ...state, todos, status: STATE_STATUSES.READY
      }
    }
    case error(FETCH_ALL_TODOS): {
      return errorReducer(state)
    }
    

    case GET_SINGLE_TODO: {
      return processReducer(state)
    }
    case success(GET_SINGLE_TODO): {
      const todo = action.payload.data

      return {
        ...state, todo, status: STATE_STATUSES.READY
      }
    }
    case error(GET_SINGLE_TODO): {
      return errorReducer(state)
    }


    case ADD_TODO: {
      return processReducer(state)
    }
    case success(ADD_TODO): {
      const todo = action.payload.data

      return {
        ...state,
        todos: [...state.todos, todo],
        status: STATE_STATUSES.READY
      }
    }
    case error(ADD_TODO): {
      return errorReducer(state)
    }


    case DELETE_TODO: {
      return processReducer(state)
    }
    case success(DELETE_TODO): {
      const todos = state.todos.filter(item => item._id !== action.meta.id)
      return {
        ...state, todos, status: STATE_STATUSES.READY
      }
    }
    case error(DELETE_TODO): {
      return errorReducer(state)
    }


    case TOGGLE_TODO: {
      return processReducer(state)
    }
    case success(TOGGLE_TODO): {
      const todos = state.todos.map(item => {
        if(item._id === action.meta.id) {
          return {
            ...item,
            done: !item.done
          }
        }

        return item
      })

      return {
        ...state,
        todos,
        status: STATE_STATUSES.READY
      }
    }
    case error(TOGGLE_TODO): {
      return errorReducer(state)
    }

    case EDIT_TODO: {
      return processReducer(state)
    }
    case success(EDIT_TODO): {
      const { title, description } = action.payload.data
      const todo = state.todo

      return {
        ...state,
        todo: {...todo, title, description},
        status: STATE_STATUSES.READY
      }
    }
    case error(EDIT_TODO): {
      return errorReducer(state)
    }


    case SEARCH_TODO: {
      return processReducer(state)
    }
    case success(SEARCH_TODO): {
      const { data } = action.payload
      
      return {
        ...state,
        todos: data,
        status: STATE_STATUSES.READY
      }
    }
    case error(SEARCH_TODO): {
      return errorReducer(state)
    }

    case TODO_FILTER: {
      const { filter } = action.payload.request.data
      return processReducer({...state, isFiltering: filter})
    }
    case success(TODO_FILTER): {
      const { data } = action.payload

      return {
        ...state,
        todos: data,
        status: STATE_STATUSES.READY
      }
    }
    case error(TODO_FILTER): {
      return errorReducer(state)
    }

    case SELECT_ALL: {
      return processReducer(state)
    }
    case success(SELECT_ALL): {
      const todos = state.todos.map(item => {
        return {
          ...item,
          done: true
        }
      })

      return {
        ...state,
        todos,
        status: STATE_STATUSES.READY
      }
    }
    case error(SELECT_ALL): {
      return errorReducer(state)
    }

    case RESELECT_ALL: {
      return processReducer(state)
    }
    case success(RESELECT_ALL): {
      const todos = state.todos.map(item => {
        return {
          ...item,
          done: false
        }
      })

      return {
        ...state,
        todos,
        status: STATE_STATUSES.READY
      }
    }
    case error(RESELECT_ALL): {
      return errorReducer(state)
    }

    default:
      return state
  }
}

const errorReducer = (state) => ({
  ...state,
  status: STATE_STATUSES.ERROR 
})

const processReducer = (state) => ({
  ...state,
  status: STATE_STATUSES.PENDING
})