import React, {Component} from 'react'
import { connect } from 'react-redux'
import Spinner from './Loader'
import Paginator from './Paginator'
import TodoBox from './TodoBox'

class TodoList extends Component {
    state = {
      per_page: 5,
      page: 0,
    }

    _paginatorChangedPage(data) {
      const page = data.selected;
      this.setState({
          page
      });
    }

    render() {
      const { todos } = this.props
      const { page, per_page } = this.state

      const limit = ((page * per_page) + per_page < todos.length) ? (page * per_page) + per_page : todos.length
      const todosRender = todos.slice(page * per_page, limit)

      return (
        <>
          {
            todos.length
            ?
            <div>
              {
                todosRender.map(todo => {return <TodoBox key={todo._id} todo={todo}/>})
              }
              
              <Paginator
                className={"console-paginator"}
                pageCount={todos.length / per_page}
                pageChanged={(data) => this._paginatorChangedPage(data)}
                forcePage={page}
              />
            </div>
            :
            <Spinner />
          }
        </>
      )
    }
}

export default connect(
  state => ({
    todos: state.reducerMain.todos,
  })
)(TodoList)
