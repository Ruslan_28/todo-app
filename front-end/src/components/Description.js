import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

import { getSingleTodo, editTodo } from '../store/todos-main/action-main'
import { formatDate } from '../helpers/FormatDate'

const Styles = styled.div`
    .wrapper-description {
        max-width: 600px;
        width: 100%;
        margin: 0 auto;
        background: #f2f2f2;
        border-radius: 6px;
        padding: 25px;

        .title-box {
            display: flex;
            align-items: center;
            justify-content: space-between;
            
            .control-btn {
                max-width: 220px;
                width: 100%;
                display: flex;
                justify-content: space-between;
                align-items: center;

                .back-link {
                    background: #5c96cc;
                    color: #fff;
                    padding: 6px 26px;
                    box-sizing: border-box;
                    border-radius: 4px;
                    font-weight: bold;
                }

                .edit {
                    padding: 6px 20px;
                    background: #ffa500;
                    font-size: 16px;
                    border: none;
                    outline: none;
                    color: #fff;
                    border-radius: 4px;
                    font-weight: bold;
                    cursor: pointer;
                }
            }

            .todo-date {
                display: inline-block;
                padding: 8px;
                box-sizing: border-box;
                background: #46a346;
                color: #fff;
                font-size: 12px;
                font-weight: bold;
                border-radius: 4px 4px 0 0;
            }
        }

        .box-description {
            margin-top: 50px;

            .title {
                margin-bottom: 15px;
            }

            .description {
                line-height: 28px;
            }

            .edit-form {
                .input, .textarea {
                    width: 100%;
                    box-sizing: border-box;
                    height: 32px;
                    padding: 6px 12px;
                    color: rgba(0,0,0,0.65);
                    font-size: 14px;
                    line-height: 1.5;
                    background: #fff;
                    background-image: none;
                    border: 1px solid #d9d9d9;
                    border-radius: 4px;
                    outline: none;
                    transition: all .3s;
                    &:focus {
                    border-color: #1890ff;
                    }
                }

                .textarea {
                    height: 160px;
                    margin: 25px 0;
                }

                .submit {
                    width: 100%;
                    color: #fff;
                    background: #1890ff;
                    border-color: #1890ff;
                    text-shadow: 0 -1px 0 rgba(0,0,0,0.12);
                    box-shadow: 0 2px 0 rgba(0,0,0,0.045);
                    height: 32px;
                    padding: 0 30px;
                    font-size: 14px;
                    border-radius: 4px;
                    border: none;
                    cursor: pointer;
                    font-weight: bold;
                    outline: none;
                    transition: all .3s;
                    &:hover {
                    background: #1581e5;
                    }
                }
            }
        }
    }
`

class Description extends Component {
    state = {
        isEdit: true,
        title: '',
        description: ''
    }

    componentDidMount() {
        const { match: { params }, getSingleTodo } = this.props;

        getSingleTodo(params.id)
    }

    componentDidUpdate(prevProps) {
        const { title, description } = this.props.todo

        if (this.props !== prevProps) {
            this.setState({ title: title, description: description })
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
          })
    }

    handleSubmit = (e) => {
        e.preventDefault()

        const { title, description } = this.state
        const { _id } = this.props.todo
        if(title && description) {
            this.props.editTodo(_id, title, description)
            this.setState({ isEdit: true, })
        }
    }

    render() {
        const { todo } = this.props
        const { isEdit, title, description } = this.state

        return (
            <Styles>
                <div className="wrapper-description">
                    <div className="title-box">
                        <div className="control-btn">
                            <Link className="back-link" to="/">Back</Link>
                            <button className="edit" onClick={() => this.setState({ isEdit: !isEdit })  }>Edit</button>
                        </div>
                        <p className="todo-date">{formatDate(new Date(todo.date))}</p>
                    </div>
                    <div className="box-description">
                        {
                            isEdit ?
                            <div>
                                <p className="title">{todo.title}</p>
                                <p className="description">{todo.description}</p>
                            </div>
                            :
                            <form className="edit-form" onSubmit={this.handleSubmit}>
                                <input
                                className="input"
                                placeholder="Title"
                                name="title"
                                value={title}
                                onChange={this.handleChange}
                                />
                                <textarea 
                                className="textarea"
                                name="description"
                                placeholder="Description"
                                value={description}        
                                onChange={this.handleChange}  
                                />
                                <button className="submit" type="submit">Submit</button>
                            </form>
                        }
                    </div>
                </div>
            </Styles>
        )
    }
}

export default connect (
    state => ({
        todo: state.reducerMain.todo
    }), { getSingleTodo, editTodo }
)(withRouter(Description))