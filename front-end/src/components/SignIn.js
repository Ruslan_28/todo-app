import React from 'react'
import { Form, Field } from 'react-final-form'
import { validationEmail, validationPassword } from '../utils/validationController';
import TextField from './TextField'

export default (props) => {

  const onValidate = values => {
      const { email, password } = values;
      const errors = {};

      errors.email = validationEmail(email);
      errors.password = validationPassword(password);

      return errors;
  }

  const onSubmit = values => {
      console.log({values})
      const { email, password } = values
      props.requestSignIn(email, password)
  }

  return (
    <Form
      onSubmit={onSubmit}
      validate={onValidate}
      render={({ handleSubmit, pristine, invalid }) => (
        <form onSubmit={handleSubmit}>
            <p>SignUp</p>
            <Field name="email" type="email" placeholder="Email">
              {props => (
                <>
                  <TextField
                    meta={props.meta}
                    name={props.input.name}
                    type={props.input.type}
                    placeholder={props.placeholder}
                    value={props.input.value}
                    onChange={props.input.onChange}
                  />
                </>
              )}
            </Field>
            <Field name="password" type="password" placeholder="Password">
              {props => (
                <>
                  <TextField
                    meta={props.meta}
                    name={props.input.name}
                    type={props.input.type}
                    placeholder={props.placeholder}
                    value={props.input.value}
                    onChange={props.input.onChange}
                  />
                </>
              )}
            </Field>
            <button className={`${(pristine || invalid) ? 'disabled-btn' : 'active-btn'}`} type="submit">Submit</button>
            {props.errorMessage && <span className="error-message">{props.errorMessage}</span>}
        </form>
      )}
    />
  )
}
