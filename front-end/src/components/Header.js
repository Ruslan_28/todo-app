import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { NavLink } from "react-router-dom";

import { requestLogOut } from '../store/todos-notification/action-notification'

const Styles = styled.div`
    background: #74bcff;
    padding: 25px;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    text-align: center;
    margin-bottom: 50px;

    .wrraper-header {
        max-width: 600px;
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;

        .links-box {
            max-width: 200px;
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: center;
            font-weight: bold;
        }

        .log-out {
            padding: 6px 14px;
            background: #c90047;
            border: none;
            outline: none;
            color: #fff;
            border-radius: 4px;
            font-weight: bold;
            font-size: 14px;
            cursor: pointer;
        }
    }
    
    .active {
        color: #c90047;
    }
`

const Header = (props) => {
    return (
        <Styles>
            {   
                props.isToken.length
                ?
                <div className="wrraper-header">
                    <div className="links-box">
                        <NavLink exact={true}  activeClassName='active' to="/" >Todo List</NavLink>
                        <NavLink activeClassName='active' to="/add" >Added Todo</NavLink>
                    </div>
                    <button className="log-out" onClick={props.requestLogOut}>Log Out</button>
                </div>
                :
                <p>Authorization</p>
            }
        </Styles>
    )
}

export default connect(
    state => ({
        isToken: state.reducerNotification.isToken,
    }), { requestLogOut }
)(Header)

