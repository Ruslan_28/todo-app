import React from 'react'
import Loader from 'react-loader-spinner'
import styled from 'styled-components'

const LoaderBox = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 150px;
`

const Spinner = (props) => {
  return (
    <LoaderBox>
      <Loader
       type="Bars"
       color="#000"
       height="60"
       width="60"
     />
    </LoaderBox>
  )
}

export default Spinner
