import React from 'react'
import styled from 'styled-components'
import { Link } from "react-router-dom";
import { Icon } from 'antd'

const Styles = styled.div`
  .wrapper-todo {
    max-width: 600px;
    width: 100%;
    margin: 35px auto;
  }

  .todo-item {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 12px;
    box-sizing: border-box;
    border-radius: 0 6px 6px 6px;
    background: ${props => props.done ? '#fbfbfb' : '#f2f2f2'};
    transition: all .5s;
  }

  .title-todo {
    font-size: 18px;
    color: #333;
    margin: 0 25px;
    text-align: center;
    text-decoration: ${props => props.done ? 'line-through' : 'none'};
    color: ${props => props.done ? '#ccc' : '#333'};
    transition: all .5s;
  }

  .icon-box {
    position: relative;
    top: 2px;
    cursor: pointer;
    font-size: 20px;
  }
`

const ItemTodo = (props) => {
  const { title, done, _id: id } = props

  return (
    <Styles done={done}>
      <div className="wrapper-todo">
        <div className="todo-item">
          <span className="icon-box" onClick={() => props.toggleTodo(id, done)}>
            <Icon type={done ? 'check-square' : 'border'} />
          </span>
          <Link to={`/description/${id}`} className="title-todo">{title}</Link>
          <span className="icon-box" onClick={() => props.deleteTodo(id)}>
            <Icon type="delete" />
          </span>
        </div>
      </div>
    </Styles>
    
  )
}

export default ItemTodo
