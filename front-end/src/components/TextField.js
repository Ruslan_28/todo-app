import React from 'react'

export default (props) => {
  const isInvalid = props.meta && props.meta.error && props.value !== "";

  return (
    <>
      <input
          name={props.name}
          type={props.type}
          placeholder={props.placeholder}
          value={props.value}
          onChange={props.onChange}
      />
      {isInvalid && ( <div className="validation">{props.meta.error}</div> )}
    </>
  )
}
