import React from 'react'
import { Form, Field } from 'react-final-form'
import { validationEmail, validationPassword, validationConfirmPassword, validationName } from '../utils/validationController';
import TextField from './TextField'

export default (props) => {

  const onValidate = values => {
      const { username, email, password, confirmPassword } = values;
      const errors = {};

      errors.username = validationName(username);
      errors.email = validationEmail(email);
      errors.password = validationPassword(password);
      errors.confirmPassword = validationConfirmPassword(password, confirmPassword);

      return errors;
  }

  const onSubmit = values => {
      const { username, email, password } = values
      props.requestSignUp(username, email, password)
  }

  return (
    <Form
      onSubmit={values => onSubmit(values)}
      validate={values => onValidate(values)}
      render={({ handleSubmit, pristine, invalid }) => (
        <form onSubmit={handleSubmit}>
            <p>SignUp</p>
            <Field name="username" type="text" placeholder="Username">
              {props => (
                <>
                  <TextField
                    meta={props.meta}
                    name={props.input.name}
                    type={props.input.type}
                    placeholder={props.placeholder}
                    value={props.input.value}
                    onChange={props.input.onChange}
                  />
                </>
              )}
            </Field>

            <Field name="email" type="email" placeholder="Email">
              {props => (
                <>
                  <TextField
                    meta={props.meta}
                    name={props.input.name}
                    type={props.input.type}
                    placeholder={props.placeholder}
                    value={props.input.value}
                    onChange={props.input.onChange}
                  />
                </>
              )}
            </Field>

            <Field name="password" type="password" placeholder="Password">
              {props => (
                <>
                  <TextField
                    meta={props.meta}
                    name={props.input.name}
                    type={props.input.type}
                    placeholder={props.placeholder}
                    value={props.input.value}
                    onChange={props.input.onChange}
                  />
                </>
              )}
            </Field>

            <Field name="confirmPassword" type="password" placeholder="Confirm password">
              {props => (
                <>
                  <TextField
                    meta={props.meta}
                    name={props.input.name}
                    type={props.input.type}
                    placeholder={props.placeholder}
                    value={props.input.value}
                    onChange={props.input.onChange}
                  />
                </>
              )}
            </Field>
            <button className={`${(pristine || invalid) ? 'disabled-btn' : 'active-btn'}`} type="submit">Submit</button>
            {props.errorMessage && <span className="error-message">{props.errorMessage}</span>}
        </form>
      )}
    />
  )
}
