import React from 'react'
import { connect } from 'react-redux'

import ItemTodo from './ItemTodo'
import { deleteTodo, toggleTodo } from '../store/todos-main/action-main'

const TodoBox = (props) => {

  const { todo } = props

  return (
    <ItemTodo
      {...todo}
      toggleTodo={props.toggleTodo}
      deleteTodo={props.deleteTodo}
    />
  )
}

export default connect(
  null, { deleteTodo, toggleTodo }
)(TodoBox)
