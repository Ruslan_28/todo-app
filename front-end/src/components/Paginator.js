import React from "react";
import ReactPaginate from 'react-paginate';

const Paginator = (props) => (
    <ReactPaginate
        pageCount={props.pageCount ? props.pageCount : 1}
        initialPage={props.initialPage ? props.initialPage : 0}
        forcePage={props.forcePage ? props.forcePage : 0}
        pageRangeDisplayed={props.pageRangeDisplayed ? props.pageRangeDisplayed : 5}
        marginPagesDisplayed={props.marginPagesDisplayed ? props.marginPagesDisplayed : 1}
        onPageChange={(data) => props.pageChanged(data)}
        containerClassName={props.className}
        previousLabel={'previous'}
        nextLabel={'next'}
    />
);

export default Paginator;