import React from 'react'
import { connect } from 'react-redux'
import { selectAll, reselectAll } from '../store/todos-main/action-main'
import styled from 'styled-components';
import { Icon } from 'antd'

const SelectWrapper =  styled.div`
    width: 80px;
    display: flex;
    justify-content: space-between;
    margin-left: 12px;

    i {
        font-size 22px;
        cursor: pointer;
        outline: none;
    }
`

const Select = ({ selectAll, reselectAll }) => {
    return (
        <SelectWrapper>
            <Icon 
                onClick={selectAll}
                type='check-square' 
            />
            <Icon
                onClick={reselectAll} 
                type='border'
            />
        </SelectWrapper>
    )
}

export default connect(
    null,
    { selectAll, reselectAll }
)(Select)