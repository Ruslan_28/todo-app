import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components';

import { searchTodo } from '../store/todos-main/action-main'

const SeachTodo = styled.input`
  width: 232px;
  height: 26px;
  padding: 14px 8px;
  box-sizing: border-box;
  outline: none;
  color: rgba(0,0,0,0.65);
  font-size: 14px;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  transition: all .3s;
  &:focus {
    border-color: #1890ff;
  }
`
const Search = (props) => {

  const handleChange = (e) => {
    props.searchTodo(e.target.value)
  }

  return (
    <div>
      <SeachTodo
        placeholder="Search todo"
        onChange={handleChange}
      />
    </div>
  )
}

export default connect(
  null,
  { searchTodo }
)(Search)
