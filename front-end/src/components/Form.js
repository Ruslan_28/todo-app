import React, { useState } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { useAlert } from 'react-alert'

import { addTodo } from '../store/todos-main/action-main'

const Styles = styled.div`
  max-width: 600px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;

  .input, .textarea {
    width: 100%;
    box-sizing: border-box;
    height: 32px;
    padding: 6px 12px;
    color: rgba(0,0,0,0.65);
    font-size: 14px;
    line-height: 1.5;
    background: #fff;
    background-image: none;
    border: 1px solid #d9d9d9;
    border-radius: 4px;
    outline: none;
    transition: all .3s;
    &:focus {
      border-color: #1890ff;
    }
  }

  .textarea {
    height: 160px;
    margin: 25px 0;
  }

  .submit {
    width: 100%;
    color: #fff;
    background: #1890ff;
    border-color: #1890ff;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.12);
    box-shadow: 0 2px 0 rgba(0,0,0,0.045);
    height: 32px;
    padding: 0 30px;
    font-size: 14px;
    border-radius: 4px;
    border: none;
    cursor: pointer;
    font-weight: bold;
    outline: none;
    transition: all .3s;
    &:hover {
      background: #1581e5;
    }
  }
`

const Form = (props) => {

  const alert = useAlert()

  const [ form, setValue ] = useState({ title: '', description: '' })

  const handleSubmit = (e) => {
    e.preventDefault()

    if(form.title && form.description) {
      props.addTodo(form.title, form.description, props.userId)
        .then(() => alert.success(<div style={{ fontSize: '14px', textTransform: 'capitalize' }}>Note successfully added!</div>))
        .catch(() => alert.error(<div style={{ fontSize: '14px', textTransform: 'capitalize' }}>Note error added!</div>))

      setValue({
        ...form,
        title: '', 
        description: ''
      })
    }
  }

  const handleChange = (e) => {
    setValue({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  return (
    <Styles>
      <form onSubmit={handleSubmit}>
        <input
          className="input"
          placeholder="Add Title"
          name="title"
          value={form.title}
          onChange={handleChange}
        />
        <textarea 
          className="textarea"
          name="description"
          placeholder="Add Description"
          value={form.description}        
          onChange={handleChange}  
        />
        <button className="submit" type="submit">Submit</button>
      </form>
    </Styles>
  )
}

export default connect(
  state => ({
    userId: state.reducerNotification.userId,
  }), { addTodo }
)(Form)
