import React from 'react'
import styled from 'styled-components';
import { connect } from 'react-redux'
import { todoFilter } from '../store/todos-main/action-main'

const WrapperFilter = styled.div`
  max-width: 218px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: #fff;
  border-radius: 8px;
  border: 2px solid #74bcff;
  overflow: hidden;

  span {
    width: 80px;
    font-size: 14px;
    cursor: pointer;
    color: #000;
    padding: 8px 6px;
    box-sizing: border-box;
    text-align: center;
  }

  .active-parm {
    color: #fff;
    background: #74bcff;
  }
`

const Filters = ({isFiltering, todoFilter}) => (
      <WrapperFilter >
        <span
          className={ isFiltering === 'NO_ACTIVE' ? 'active-parm' : '' }
          onClick={() => todoFilter('NO_ACTIVE')}
        >
          No Done
        </span>
        <span
          className={ isFiltering === 'ACTIVE' ? 'active-parm' : '' }
          onClick={() => todoFilter('ACTIVE')}
        >
          Done
        </span>
        <span
          className={ isFiltering === 'ALL' ? 'active-parm' : '' }
          onClick={() => todoFilter('ALL')}
        >
          All
        </span>
      </WrapperFilter>
)

export default connect(
  state => ({
    isFiltering: state.reducerMain.isFiltering
  }),
  { todoFilter }
)(Filters)
